﻿
using System.Security.Policy;
using System.Text;
using Newtonsoft.Json;
using paginaMisministros.Models;

namespace paginaMisministros.ConexionApi
{
    public class ServicioTitulos
    {

        public async  Task< List<Titulo>> getTitulos() { 
        List<Titulo> listitulos = new List<Titulo>();
        var url = "http://localhost:5146/api/Titulos_Aspirantes/Obtener";
            using var apirest = new HttpClient();
    HttpResponseMessage respuesta = await apirest.GetAsync(url);



            if (respuesta.IsSuccessStatusCode)
            {
                var Respuesta = respuesta.Content.ReadAsStringAsync().Result;
                listitulos = JsonConvert.DeserializeObject<List<Titulo>>(Respuesta);
            }

return listitulos;



        }
        public async Task<Titulo> Buscar(String id)
        {


            var titulo = new Titulo();
            var url = "http://localhost:5146/api/Titulos_Aspirantes/" + id;
            using var apirest = new HttpClient();
            HttpResponseMessage respuesta = await apirest.GetAsync(url);



            if (respuesta.IsSuccessStatusCode)
            {
                var Respuesta = respuesta.Content.ReadAsStringAsync().Result;
                titulo = JsonConvert.DeserializeObject<Titulo>(Respuesta);

                return titulo;


            }
            return null;
        }
public async Task<bool> Insertar(Titulo titulo)
{
    String _baseurl = "http://localhost:5146/";
    bool Respuesta = false;
    var cliente = new HttpClient();
    cliente.BaseAddress = new Uri(_baseurl);

    var contenido = new StringContent(JsonConvert.SerializeObject(titulo), Encoding.UTF8, "application/json");
    var response = await cliente.PostAsync($"api/Titulos_Aspirante/", contenido);

    if (response.IsSuccessStatusCode)
    {
        Respuesta = true;
    }
    return Respuesta;
}


    }
}
