﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using paginaMisministros.Models;
using System.Text;

namespace paginaMisministros.ConexionApi
{
    public class Conexion
    {
        public async Task<List<Oferente>> getOferentes() {

            List<Oferente> listaoferentes = new List<Oferente>();
            var url = "http://localhost:5146/api/OferentesApi/Obtener";
            using var apirest = new HttpClient();
            HttpResponseMessage respuesta = await apirest.GetAsync(url);



            if (respuesta.IsSuccessStatusCode)
            {
                var Respuesta = respuesta.Content.ReadAsStringAsync().Result;
                listaoferentes = JsonConvert.DeserializeObject<List<Oferente>>(Respuesta);
            }

            return listaoferentes;



        }
        public async  Task<Oferente> Buscar(String id) {


           Oferente oferente= new Oferente();
            var url = "http://localhost:5146/api/OferentesApi/"+id;
            using var apirest = new HttpClient();
            HttpResponseMessage respuesta = await apirest.GetAsync(url);



            if (respuesta.IsSuccessStatusCode)
            {
                var Respuesta = respuesta.Content.ReadAsStringAsync().Result;
                oferente = JsonConvert.DeserializeObject<Oferente>(Respuesta);
            }

            return oferente;


        }
        public async Task <bool> Insertar(Oferente oferente) {
            String _baseurl = "http://localhost:5146/";
            bool Respuesta = false;
            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri(_baseurl);
           
            var contenido = new StringContent(JsonConvert.SerializeObject(oferente), Encoding.UTF8, "application/json");
            var response = await cliente.PostAsync($"api/OferentesApi/", contenido);

            if (response.IsSuccessStatusCode)
            {
                Respuesta = true;
            }
            return Respuesta;
         }


    }

}

