﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace paginaMisministros.Models
{
    public class Oferente
    {
        [Key]
        [Required(ErrorMessage = "El campo no puede estar vacio")]
         
        [RegularExpression("^([0][1-9]-[0-9]{4}-[0-9]{4})$",ErrorMessage = "Formato de cedula  invalido Fisica = 0#-####-####   Juridica =#-###-######")]
        public String cedula { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [StringLength(60, MinimumLength = 10, ErrorMessage = "El nombre debe tener entre 10 y 60 caracteres")]

        public String Nombre { get; set; }
        [Required(ErrorMessage = "El campo no puede estar vacio")]
        [Range(1900, 2005, ErrorMessage = "debe selecionar una fecha valida")]
        public int FechaNacimiento { get; set; }
        [Required(ErrorMessage = "El campo no puede estar vacio")]

        [RegularExpression(@"^([0-9]{4}[-][0-9]{4})$", ErrorMessage = "Numero de telefono invalido")]
        public String Telefono { get; set; }
        [Required(ErrorMessage = "Campo obligatorio")]
        [EmailAddress(ErrorMessage = "Correo no valido")]
        public String Correo { get; set; }
        public String Estado { get; set; }
        [Required(ErrorMessage ="Campo obligatorio")]
        public String Puesto { get; set; }
        [Required(ErrorMessage ="Este Campo es obligatorio")]
        [Range(0,100000000)]
        public double PretensionSalarial { get; set; }
       
        [ForeignKey("Oferentecedula")]
        public List<Titulo> listaUniversidad { get; set; } = new List<Titulo>();

        [ForeignKey("Oferentecedula")]
        public List<Experiencia_Laboral> listexperiencia { get; set; } = new List<Experiencia_Laboral>();
    
    
    }



}
