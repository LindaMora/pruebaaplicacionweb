﻿using System.ComponentModel.DataAnnotations;

namespace paginaMisministros.Models
{
    public class Titulo
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Campo obligatorio")]
        [StringLength(150, MinimumLength = 10, ErrorMessage = "Maximo 60 caracteres, minimo 10 caracteres")]
        public String GradoAcademico { get; set; }
        [Required(ErrorMessage ="Campo requerido")]
        [StringLength(60, MinimumLength = 10, ErrorMessage = "Madimo 60 caracteres, minimo 10 caracteres")]
        public String CentroEstudio { get; set; }
        [Required (ErrorMessage="Campo requerido")]
        [Range(1900, 2022, ErrorMessage = "Error de rango titulo universitario")]
        public int AnioTitulo { get; set; }
        public String? Oferentecedula { get; set; }
    }
}
