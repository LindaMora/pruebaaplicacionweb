﻿using System.ComponentModel.DataAnnotations;

namespace paginaMisministros.Models
{
    public class Experiencia_Laboral
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        [StringLength(60, MinimumLength = 10, ErrorMessage = "Error  el rango debe estar entre 10 y 60 caracteres")]
        public String NombreEmpresa { get; set; }
        [Required(ErrorMessage = "Campo obligatorio")]
        [Range(1900, 2022, ErrorMessage = "Rango Invalido")]
        public int AnioInicio { get; set; }
        public int AnioFin { get; set; }
        public String? Oferentecedula { get; set; }


    }
}
