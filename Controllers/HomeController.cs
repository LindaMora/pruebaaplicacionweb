﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using paginaMisministros.ConexionApi;
using paginaMisministros.Models;
using System.Diagnostics;

namespace paginaMisministros.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private static String id = "";
        private ServicioTitulos ititulo;
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            ititulo = new ServicioTitulos();
        }



        public async Task <ActionResult>Oferentes()
        {   Conexion cn =new Conexion ();   
            List<Oferente> lista = null;
            lista = await cn.getOferentes();    
            return View(lista);
        }
        public ActionResult Registro() {
           Oferente oferente = new Oferente();
            oferente.listaUniversidad.Add(new Titulo { Id = 0 });
           oferente.listexperiencia.Add(new Experiencia_Laboral { Id = 0 });
           ViewBag .Ministerios=getMinisterios();   
            
            return View(oferente);
        
        }

        // registro d clientes
        [HttpPost]

        public async Task<ActionResult> Registro(Oferente oferente,string opcion) {
            oferente.listaUniversidad[0].Oferentecedula = oferente.cedula;

            oferente.listexperiencia[0].Oferentecedula =oferente.cedula;
            oferente.Puesto = opcion;
            
             Conexion cn = new Conexion();
           bool respuesta=await  cn.Insertar(oferente);

            if (respuesta == true) { id = oferente.cedula; }

            return RedirectToAction("Registro");
        }
      

        public async Task<ActionResult> Buscar(String id = "")
        {
            Conexion cn = new Conexion();
            if (!String.IsNullOrEmpty(id)) {
             
                List<Oferente> listoferente = new List<Oferente>();
              listoferente.Add (await cn.Buscar(id));
                return View(listoferente);
            }
            return View();
        }

        public async Task<ActionResult> Actualizar(String id) {
            Conexion cn = new Conexion();
            var oferente = cn.Buscar(id);
            ViewBag.Ministerios = getMinisterios();
            return View(oferente);
        }

        [HttpPost]



        public async Task<ActionResult> Actualizar(Oferente oferente )
        {
            Conexion cn = new Conexion();
            // var respuesta= cn.;
            return View();

            return View(oferente);
        }

        public async Task<ActionResult> AgregarTitulo() {

            ViewBag.Cedula = id;
            return View();
        }

        [HttpPost]

        public async Task<ActionResult> AgregarTitulo([FromBody] Titulo titulo)
        {
     
            return View();
        }

        // funcion para llenar  el listado de Ministerios 

        public List<SelectListItem> getMinisterios() {

            return new List<SelectListItem> {
            new SelectListItem{Text="Ministerio de Agricultura y ganaderia (MAG)"},
            new SelectListItem{Text="Instituto CostaRicense de Turismo (ICT)"},
            new SelectListItem{ Text="Ministerio de Obras publicas y Tramsportes(MOP)"},
            new SelectListItem{Text="Ministerio de Salud" },
            new SelectListItem{Text="Ministerio de Obras publicas y Tramsportes (MOP)"},
            new SelectListItem{Text="Ministerio de Ciencia y Tecnologia (MICIT)"},
            new SelectListItem{ Text="Ministerio de Seguridad Publica "},
            new SelectListItem{Text="Ministerio de Educacion Publica (MEP)" }

            };
        
        }
        

        /*    
         @Html.dropdownList("Opciones"(List<SelectListItem>)@ViewBag.Ministerios)
         
         */

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}